'use strict'

const { test, trait } = use('Test/Suite')('Agendamento')

trait('Test/ApiClient')

const base_url = 'agendamentos';
const column_edit = {"data_hora" : "2018-11-10 13:00:00"}
const body_especialidade = {
  "descricao" : "Especialidade 1"
}
const body_medico = {
  "nome_completo" : "Medico 1",
  "crm" : "123"
}
const body_paciente = {
  "nome_completo" : "Paciente 1",
  "cpf" : "00747559279"
}
const body = {
	"id_medico" : '',
	"id_paciente" : '',
	"data_hora" : "2018-11-13 13:00:00"
}
const filters = { ...body, limit: 1 }

test('store_especialidade', async({ client }) => {
  var response = await client
  .post('especialidades')
  .send(body_especialidade)
  .header('accept', 'application/json')
  .end()
  body_medico.id_especialidade = response.body.data.id;
  
  response.assertStatus(200)
  response.assertJSONSubset({ data: { ...body_especialidade } })
})

test('store_medicos', async ({ client }) => {
  
  var response = await client
    .post('medicos')
    .send(body_medico)
    .header('accept', 'application/json')
    .end()
    
    body.id_medico = response.body.data.id;
    filters.id_medico = response.body.data.id;
    filters.medico = response.body.data

  response.assertStatus(200)
  response.assertJSONSubset({ data: { ...body_medico } })

})

test('store_paciente', async ({ client }) => {
  
  var response = await client
    .post('pacientes')
    .send(body_paciente)
    .header('accept', 'application/json')
    .end()
    
    body.id_paciente = response.body.data.id;
    filters.id_paciente = response.body.data.id;
    filters.paciente = response.body.data;

  response.assertStatus(200)
  response.assertJSONSubset({ data: { ...body_paciente } })

})

test('store', async ({ client }) => {
  
  var response = await client
    .post(base_url)
    .send(body)
    .header('accept', 'application/json')
    .end()
    
    body.id = response.body.data.id;
    filters.id = response.body.data.id;

  response.assertStatus(200)
  response.assertJSONSubset({ data: { ...body } })

})

test('update', async ({ client }) => {
  const key = Object.keys(column_edit)[0]
  body[key] = column_edit[key]

  var response = await client
    .put(base_url +"/" + body.id)
    .send(body)
    .header('accept', 'application/json')
    .end()

  response.assertStatus(200)
  response.assertJSONSubset({ data: { ...body } })

})

test('index', async ({ client }) => {
  
  const response = await client
    .get(base_url)
    .header('accept', 'application/json')
    .query(filters)
    .end()

    body.paciente = filters.paciente;
    body.medico = filters.medico;
    
  response.assertStatus(200)
  response.assertJSONSubset({ data: { data: [{ ...body }] } })

})

test('show', async ({ client }) => {
  
  var response = await client
    .get(base_url + '/' + body.id)
    .end()
  
  response.assertStatus(200)
  response.assertJSONSubset({ data: { ...body } })

})

test('destroy', async ({ client }) => {
  
  const response = await client
      .delete(base_url + '/' + body.id)
      .header('accept', 'application/json')
      .end()

  response.assertJSONSubset({ data: 'ModelNotFoundException' })

})