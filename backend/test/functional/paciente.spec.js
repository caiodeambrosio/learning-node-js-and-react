'use strict'

const { test, trait } = use('Test/Suite')('Paciente')

trait('Test/ApiClient')

const base_url = 'pacientes';
const column_edit = {"nome_completo" : "Update"}
const body = {
  "nome_completo" : "Paciente 1",
  "cpf" : "00747559279"
}
const filters = { ...body, limit: 1 }

test('store', async ({ client }) => {
  
  var response = await client
    .post(base_url)
    .send(body)
    .header('accept', 'application/json')
    .end()
  
    body.id = response.body.data.id;
    filters.id = response.body.data.id;

  response.assertStatus(200)
  response.assertJSONSubset({ data: { ...body } })

})

test('index', async ({ client }) => {
  
  const response = await client
    .get(base_url)
    .header('accept', 'application/json')
    .query(filters)
    .end()
    
  response.assertStatus(200)
  response.assertJSONSubset({ data: { data: [{ ...body }] } })

})

test('show', async ({ client }) => {
  
  var response = await client
    .get(base_url + '/' + body.id)
    .end()
  
  response.assertStatus(200)
  response.assertJSONSubset({ data: { ...body } })

})

test('update', async ({ client }) => {
  
  const key = Object.keys(column_edit)[0]
  body[key] = column_edit[key]
  
  var response = await client
    .put(base_url +"/" + body.id)
    .send(body)
    .header('accept', 'application/json')
    .end()

  response.assertStatus(200)
  response.assertJSONSubset({ data: { ...body } })

})

test('Delete', async ({ client }) => {
  
  const response = await client
      .delete(base_url + '/' + body.id)
      .header('accept', 'application/json')
      .end()

  response.assertJSONSubset({ data: 'ModelNotFoundException' })

})


