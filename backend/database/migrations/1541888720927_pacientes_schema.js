'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PacientesSchema extends Schema {
  up () {
    this.create('pacientes', (table) => {
      table.increments()
      table.string('nome_completo', 255).notNullable()
      table.string('cpf', 11).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('pacientes')
  }
}

module.exports = PacientesSchema
