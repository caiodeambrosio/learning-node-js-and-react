'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MedicosSchema extends Schema {
  up () {
    this.create('medicos', (table) => {
      table.increments()
      table.string('nome_completo', 255).notNullable()
      table.string('crm', 255).notNullable()
      table.integer("id_especialidade").notNullable().unsigned().index();
      table.foreign('id_especialidade').references('id').on('especialidades').onDelete('cascade')
      table.timestamps()
    })
  }

  down () {
    this.drop('medicos')
  }
}

module.exports = MedicosSchema
