'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AgendamentoSchema extends Schema {
  up () {
    this.create('agendamentos', (table) => {
      table.increments()
      table.integer("id_medico").notNullable().unsigned().index();
      table.foreign('id_medico').references('id').on('medicos').onDelete('cascade')
      table.integer("id_paciente").notNullable().unsigned().index();
      table.foreign('id_paciente').references('id').on('pacientes').onDelete('cascade')
      table.datetime('data_hora')
      table.timestamps()
    })
  }

  down () {
    this.drop('agendamentos')
  }
}

module.exports = AgendamentoSchema
