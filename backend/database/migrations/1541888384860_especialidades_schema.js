'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EspecialidadesSchema extends Schema {
  up () {
    this.create('especialidades', (table) => {
      table.increments()
      table.string('descricao').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('especialidades')
  }
}

module.exports = EspecialidadesSchema
