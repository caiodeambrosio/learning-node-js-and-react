'use strict'

const Route = use('Route')

Route.resource("posts", "PostController").apiOnly();
Route.resource("especialidades", "EspecialidadeController").apiOnly();
Route.resource("pacientes", "PacienteController").apiOnly();
Route.resource("medicos", "MedicoController").apiOnly();
Route.resource("agendamentos", "AgendamentoController").apiOnly();
