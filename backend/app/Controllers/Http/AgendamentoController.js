'use strict'
const CoreController = require('./CoreController')

class AgendamentoController extends CoreController {
  constructor() {
    const model = 'Agendamento';
    const store_request_body = ['id_medico', 'id_paciente', 'data_hora'];
    const update_request_body = ['id_medico', 'id_paciente', 'data_hora'];
    const index_request_body = {
      'id' : '=', 
      'id_medico' : '=', 
      'id_paciente' : '=',
      'nome_medico' : 'like',
      'nome_paciente' : 'like'
    };

    super(model, store_request_body, update_request_body, index_request_body);
  }

  async index ({ request, response }) {
    const body_request = request.only(Object.keys(this.index_request_body));

    Object.keys(this.index_request_body).forEach(function(key) {        
      body_request[key] = body_request[key] ? body_request[key] : "";
    })

    let data = this.model.query()
    data.with('medico')
    data.with('paciente')
    
    if(body_request['id'] != ""){
      data.where('id', '=', body_request['id'])
    }

    if(body_request['id_medico'] != ""){
      data.where('id_medico', '=', body_request['id_medico'])
    }

    if(body_request['id_paciente'] != ""){
      data.where('id_paciente', '=', body_request['id_paciente'])
    }
    
    data.whereHas('medico', (builder) => {
      builder.where('nome_completo', 'like', '%' + body_request['nome_medico'] + '%')
    }, '>', 0)
    
    data.whereHas('paciente', (builder) => {
      builder.where('nome_completo', 'like', '%' + body_request['nome_paciente'] + '%')
    }, '>', 0)
    
    data = await data.paginate(body_request.page, body_request.limit);
    
    return response.status(200).send({ sucess: true, data: data });
    
  }

  async show ({ params, response }) {
    
    const data = await this.model.query()
    .with('medico')
    .with('paciente')
    .where('id', params.id)
    .first()
    
    return response.status(200).send({ sucess: true, data: data });
  }
}

module.exports = AgendamentoController
