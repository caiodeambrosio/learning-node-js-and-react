'use strict'

class CoreController {
    constructor(model, store_request_body, update_request_body, index_request_body) {
        this.model = use('App/Models/' + model);
        this.store_request_body = store_request_body;
        this.update_request_body = update_request_body;
        this.index_request_body = index_request_body;
        
    }

    async index ({ request, response }) {
        const body_request = request.only(Object.keys(this.index_request_body));
        
        let data = this.model.query()
        const index_body = this.index_request_body
        Object.keys(body_request).forEach(function(key) {
            
            const filter = index_body[key]
            
            body_request[key] = body_request[key] = body_request[key] ? body_request[key] : "";
            
            switch (filter){
            case '=':
                body_request[key] !== "" ? data.where(key, filter, body_request[key])  : "";
            break;
            
            case 'like':
                data.where(key, filter, "%" + body_request[key] + "%")
            break;
            
            case 'paginate':
            break;
            
            default:
                body_request[key] !== "" ? data.where(key, filter, body_request[key]) : ""
            break
            }
        })
        
        data = await data.paginate(body_request.page, body_request.limit);
        
        return response.status(200).send({ sucess: true, data: data });
        
    }

    async store ({ request, response }) {
    
        const body_request = request.only(this.store_request_body);
        
        const entity = await this.model.create(body_request);
    
        return response.status(200).send({ sucess: true, data: entity });
    
    }
      
    async show ({ params, response }) {
    
        const data = await this.model.findOrFail(params.id)
        
        return response.status(200).send({ sucess: true, data: data });
    
    }
      
    async update ({ params, request, response }) {
        
        const body_request = request.only(this.update_request_body);
        
        var data = await this.model.findOrFail(params.id)
    
        data.merge(body_request);
    
        await data.save();
    
        return response.status(200).send({ sucess: true, data: data });
    
    }
      
    async destroy ({ params, response }) {
        
        const data = await this.model.findOrFail(params.id);
    
        await data.delete();
    
        try{
            await this.model.findOrFail(params.id);
        } catch(e){
            if(e.name == 'ModelNotFoundException'){
                return response.status(200).send({ sucess: true, data: e.name });
            }
        }   
    }
    
}

module.exports = CoreController
