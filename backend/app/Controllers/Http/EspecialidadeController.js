'use strict'
const CoreController = require('./CoreController')

class EspecialidadeController extends CoreController{
  constructor() {
    const Model = 'Especialidade';
    const store_request_body = ['descricao'];
    const update_request_body = ['descricao'];
    const index_request_body = {
      'id' : '=', 
      'descricao' : 'like'
    };

    super(Model, store_request_body, update_request_body, index_request_body);
  }
    
}

module.exports = EspecialidadeController
