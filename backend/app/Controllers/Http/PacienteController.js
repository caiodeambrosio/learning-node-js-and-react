'use strict'
const CoreController = require('./CoreController')

class PacienteController extends CoreController{
  constructor() {
    const Model = 'Paciente';
    const store_request_body = ['nome_completo', 'cpf'];
    const update_request_body = ['nome_completo', 'cpf'];
    const index_request_body = {
      'id' : '=', 
      'nome_completo' : 'like', 
      'cpf' : 'like'
    };

    super(Model, store_request_body, update_request_body, index_request_body);
  }
}

module.exports = PacienteController
