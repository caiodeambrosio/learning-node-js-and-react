'use strict'
const CoreController = require('./CoreController')

class MedicoController extends CoreController {
  constructor() {
    const Model = 'Medico';
    const store_request_body = ['nome_completo', 'crm', 'id_especialidade'];
    const update_request_body = ['nome_completo', 'crm', 'id_especialidade'];
    const index_request_body = {
      'id' : '=', 
      'nome_completo' : 'like', 
      'crm' : 'like', 
      'id_especialidade' : 'like',
    };

    super(Model, store_request_body, update_request_body, index_request_body);
  }
}

module.exports = MedicoController
