'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Agendamento extends Model {
    medico () {
        return this.hasOne('App/Models/Medico')
    }

    static get dates () {
        return super.dates.concat(['data_hora'])
    }

    medico () {
        return this.belongsTo('App/Models/Medico', 'id_medico', 'id')
    }

    paciente () {
        return this.belongsTo('App/Models/Paciente', 'id_paciente', 'id')
    }
}

module.exports = Agendamento
